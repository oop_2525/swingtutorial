package com.rangsiman.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame {
    JLabel lblName;
    JTextField txtName;
    JButton btHello;
    JLabel lblHello;

    public HelloMyName() {
        super("Hello My Name");

        lblName = new JLabel("Name : ");
        lblName.setBounds(10, 10, 100, 20);
        lblName.setHorizontalAlignment(JLabel.LEFT);

        txtName = new JTextField();
        txtName.setBounds(60, 10, 200, 20);

        btHello = new JButton("Hello");
        btHello.setBounds(20, 40, 260, 20);
        btHello.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lblHello.setText("Hello " + myName);
            }
        });

        lblHello = new JLabel("Hello My Name");
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        lblHello.setBounds(20, 70, 260, 20);

        this.add(lblName);
        this.add(txtName);
        this.add(btHello);
        this.add(lblHello);
        this.setLayout(null);
        this.setSize(400, 400);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        HelloMyName helloMyName = new HelloMyName();
    }
}
