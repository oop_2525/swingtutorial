package com.rangsiman.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class ButtonExample1 extends JFrame {
    JButton button;
    JButton clearButton;
    JTextField textField;

    public ButtonExample1() {
        super("Button Example");
        textField = new JTextField();
        textField.setBounds(50, 50, 150, 20);
        button = new JButton("Welcome");
        button.setBounds(50, 100, 140, 30);
        button.setIcon(new ImageIcon("wavinghand.png"));
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Button");
                textField.setText("Welcome to Myworld");
            }
        });
        clearButton = new JButton("Clear");
        clearButton.setBounds(50, 165, 140, 30);
        clearButton.setIcon(new ImageIcon("clearButton.png"));
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                textField.setText("");
            }

        });
        this.add(textField);
        this.add(button);
        this.add(clearButton);
        this.setSize(400, 400);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
    }
}
