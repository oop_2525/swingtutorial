package com.rangsiman.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("First JFrame");
        JButton button = new JButton();
        button.setText("Click");

        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.add(button);
        frame.setLayout(null);
        button.setBounds(130, 100, 100, 40);
    }
}
