package com.rangsiman.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.plaf.FontUIResource;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueueList, lblCurrent;
    JButton btAddQueue, btGetQueue, btClearQueue;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");

        queue = new LinkedList<>();

        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btAddQueue = new JButton("Add Queue");
        btAddQueue.setBounds(250, 10, 110, 20);
        btAddQueue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btGetQueue = new JButton("Get Queue");
        btGetQueue.setBounds(250, 40, 110, 20);
        btGetQueue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();
            }
        });

        btClearQueue = new JButton("Clear Queue");
        btClearQueue.setBounds(250, 70, 110, 20);
        btClearQueue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();
            }
        });

        lblQueueList = new JLabel("Empty");
        lblQueueList.setBounds(30, 40, 200, 20);

        lblCurrent = new JLabel("?");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new FontUIResource("Serif", FontUIResource.PLAIN, 48));
        lblCurrent.setBounds(30, 70, 200, 50);

        this.add(btAddQueue);
        this.add(btGetQueue);
        this.add(btClearQueue);
        this.add(lblQueueList);
        this.add(lblCurrent);
        this.add(txtName);
        this.setLayout(null);
        this.setSize(400, 400);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        showQueue();
    }

    public void showQueue() {
        if (queue.isEmpty()) {
            lblQueueList.setText("Empty");
        } else {
            lblQueueList.setText(queue.toString());
        }
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        txtName.setText("");
        queue.add(name);
        showQueue();
    }

    public void getQueue() {
        if (queue.isEmpty()) {
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void clearQueue() {
        queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        showQueue();
    }

    public static void main(String[] args) {
        QueueApp qApp = new QueueApp();
    }
}
